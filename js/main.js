const HEXA = 16;
const BINARY = 2;

const inputDecimal = document.getElementById('decimal');
const inputBinary = document.getElementById('binary');
const inputHexa = document.getElementById('hexa');
const inputMask = document.getElementById('mask');

function decToBinary(decimal) {
    return converter(decimal, BINARY);
}

function decToHex(decimal) {
    return converter(decimal, HEXA);
}

function converter(rawSource, baseDest) {
    const source = Number(rawSource);
    return source % baseDest;
}

function decimalChange() {
    let input = inputDecimal.value;
    const [rawDecimals, mask] = getMask(input);

    const decimalsString = rawDecimals.split('.');
    const decimals = [];
    try {
        decimalsString.forEach(val => decimals.push(Number(val)));
        const binary = [];
        const hexa = [];
        decimals.forEach(val => binary.push(space(val.toString(BINARY), 4, 8)));
        decimals.forEach(val => hexa.push(space(val.toString(HEXA).toUpperCase(), 2, 2)));
    
        inputHexa.innerHTML = hexa.join(' . ');
        inputBinary.innerHTML = binary.join(' . ');
        inputMask.innerHTML = !!mask ? mask : 'Not defined';
    } catch (ex) {
        console.error(`Convertion error, decimal is not a number\n${ex}`);
    }

}

/**
 * Reverse a string
 * @param {string} stringValue 
 */
function reverse(stringValue) {
    return stringValue.split('').reverse().join('');
}

/**
 * 
 * @param {string} reverseValue 
 * @param {number} nb 
 */
function addSpace(reverseValue, nb) {
    const regex = RegExp(`(.{${nb}})`);
    let cut = reverseValue.split(regex);
    cut = cut.filter(val => val !== '');
    const spaceInCut = cut.join(' ');
    return spaceInCut;
}

/**
 * 
 * @param {string} _values 
 * @param {number} nb 
 */
function fillSpace(_values, nb) {
    if (_values.length < nb) {
        const nb0Missing = nb - (_values.length % nb);
        _values = `${'0'.repeat(nb0Missing)}${_values}`;
    }
    return _values;
}

/**
 * 
 * @param {string} rawDecimal
 */
function getMask(rawDecimal) {
    return rawDecimal.split('/');
}

/**
 * 
 * @param {string} value 
 * @param {number} nb 
 * @param {number} fill 
 */
function space(_value, nb, fill) {
    let value = _value;
    value = fillSpace(value, fill);
    value = reverse(value);
    value = addSpace(value, nb);
    value = reverse(value);
    return value;
}

function init() {
    inputDecimal.value = 0;
    decimalChange();
}

init();
